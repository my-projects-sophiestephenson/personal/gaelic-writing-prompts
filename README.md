# Gaelic Writing Prompts Generator

This is just a super simple script to help me practice Scottish Gaelic. It picks a random prompt from a list I've written and prints it out.

Maybe at some point I'll add ways for it to get other prompts from the internet, or to generate new prompts, but that didn't feel necessary right now.