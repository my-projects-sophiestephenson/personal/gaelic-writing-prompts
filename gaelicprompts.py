##
## gaelicprompts.py
## sophie stephenson
## september 2020

## a simple program that prints out a random value from a list of
## possible journal prompts - so I can practice my Gaelic skills

from random import choice

prompts = [
    "Write about your plans for the weekend.",
    "Write about what you did today.",
    "Write about three things you're thankful for and why.",
    "Tell a story about something that happened to you.",
    "Where is a place you'd like to travel to? Why?",
    "What did you do yesterday?",
    "Write about what you like and dislike about your classes at this moment.",
    "Pick something you have an opinion about and convince someone that you are right.",
    "Pick a member of your family and describe them in detail.",
    "Pick a friend of yours and describe them in detail.",
    "What was the best year of college, and why?",
    "Summarize the last Litir Bheag you transcribed. What did you like / not like about it?",
    "Describe your perfect romantic partner.",
    "What is something you like about yourself today?",
    "Introduce yourself to someone new. Write it down, then practice saying it until it comes easily.",
    "Talk about your favorite _____ (food, color, music genre, whatever you want).",
    "If you had three wishes guaranteed to come true right now, what would you wish for?",
    "Write a dialogue between you and a shop worker in Uist.",
    "Pick ten long, pretentious English words (> 4 syllables) and find their translations in Gaelic.",
    "Practice conjugating the ten irregular verbs.",
    "Pick five verbs and conjugate them in every tense. Use each one in three sentences, using three different tenses.",
    "Write a letter to someone you haven't seen in a while.",
    "Go back to a Litir Bheag and make a note of five new words you learned in that letter. Write five sentences, one that uses each word or phrase.",
    "Pick a random verb tense and look for instances of that verb tense in past Litir Bheag. Write them down, and explain how to conjugate each from the root.",
    "[Long one]: Pick a favorite song and translate that song into Gaelic. Extra challenge: see if you can make it rhyme!",
    ]

prompt = choice(prompts)
print(prompt)
